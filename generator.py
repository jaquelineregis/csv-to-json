import csv
import json

csv_path = '<file>.csv'


def get_rows(csv_path):
    with open(csv_path) as csv_file:
        fieldnames = ("header", "header")
        writer = csv.DictReader(csv_file, fieldnames)
        next(writer)

        for row in writer:
            yield row


with open('<file>.json', 'w') as file:
    json_string = json.dumps({"data": list(get_rows(csv_path))}, indent=2)
    file.write(json_string)
